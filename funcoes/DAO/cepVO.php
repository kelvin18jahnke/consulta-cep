<?php

class cepVO {
    private $idCep;
    private $numeroCep;
    private $logradouroCep;
    private $bairroCep;
    private $cidadeCep;
    private $ufCep;
    
    function getIdCep() {
        return $this->idCep;
    }

    function getNumeroCep() {
        return $this->numeroCep;
    }

    function getLogradouroCep() {
        return $this->logradouroCep;
    }

    function getBairroCep() {
        return $this->bairroCep;
    }

    function getCidadeCep() {
        return $this->cidadeCep;
    }

    function getUfCep() {
        return $this->ufCep;
    }

    function setIdCep($idCep) {
        $this->idCep = $idCep;
    }

    function setNumeroCep($numeroCep) {
        $this->numeroCep = $numeroCep;
    }

    function setLogradouroCep($logradouroCep) {
        $this->logradouroCep = $logradouroCep;
    }

    function setBairroCep($bairroCep) {
        $this->bairroCep = $bairroCep;
    }

    function setCidadeCep($cidadeCep) {
        $this->cidadeCep = $cidadeCep;
    }

    function setUfCep($ufCep) {
        $this->ufCep = $ufCep;
    }


}
