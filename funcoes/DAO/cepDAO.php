<?php
require_once 'Conexao.php';
class cepDAO extends Conexao {
    
    public function ConsultaCepBaseDados(cepVO $vo) {
        $conexao = parent::retornaConexao();
        $comando = 'select id_cep,
                           numero_cep,
                           logradouro_cep,
                           bairro_cep,
                           cidade_cep,
                           uf_cep
                      from cep where numero_cep like ?';
        
        $sql = new PDOStatement();
        $sql = $conexao->prepare($comando);
        $sql->bindValue(1, $vo->getNumeroCep() . "%");
     
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        $sql->execute();
        return $sql->fetchAll();
    }
    
     public function InserirCep(cepVO $vo) {
        $conexao = parent::retornaConexao();
        $comando = 'insert into cep (numero_cep, logradouro_cep, bairro_cep, cidade_cep, uf_cep) values(?,?,?,?,?)';
        $sql = new PDOStatement();
        $sql = $conexao->prepare($comando);
        $sql->bindValue(1, $vo->getNumeroCep());
        $sql->bindValue(2, $vo->getLogradouroCep());
        $sql->bindValue(3, $vo->getBairroCep());
        $sql->bindValue(4, $vo->getCidadeCep());
        $sql->bindValue(5, $vo->getUfCep());
        try {
            $sql->execute();
            return 1;
        } catch (Exception $ex) {
            return -1;
        }
    }
}
