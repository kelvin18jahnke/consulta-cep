<?php

require_once 'cepDAO.php';

class cepCTRL {
    //Consulta do CEP na base de dados
    public function ConsultaCepBaseDados(cepVO $vo) {
        $dao = new cepDAO();
        $cep = $dao->ConsultaCepBaseDados($vo);
        return $cep;
    }
    //Inseri o CEP no banco de dados
    public function InserirCep(cepVO $vo) {

        $dao = new cepDAO();
        $ret = $dao->InserirCep($vo);
        return $ret;
    }

}
