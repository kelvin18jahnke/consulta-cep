-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 28-Fev-2021 às 18:19
-- Versão do servidor: 10.4.17-MariaDB
-- versão do PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `db_cep`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cep`
--

CREATE TABLE `cep` (
  `id_cep` int(11) NOT NULL,
  `numero_cep` varchar(9) NOT NULL,
  `logradouro_cep` varchar(60) DEFAULT NULL,
  `bairro_cep` varchar(60) DEFAULT NULL,
  `cidade_cep` varchar(60) NOT NULL,
  `uf_cep` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cep`
--

INSERT INTO `cep` (`id_cep`, `numero_cep`, `logradouro_cep`, `bairro_cep`, `cidade_cep`, `uf_cep`) VALUES
(6, '96170-000', '', '', 'São Lourenço do Sul', 'RS'),
(8, '54360-080', 'Rua Forte das Cinco Pontas', 'Marcos Freire', 'Jaboatão dos Guararapes', 'PE'),
(9, '54360-060', 'Rua Arraial do Bom Jesus', 'Marcos Freire', 'Jaboatão dos Guararapes', 'PE');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cep`
--
ALTER TABLE `cep`
  ADD PRIMARY KEY (`id_cep`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cep`
--
ALTER TABLE `cep`
  MODIFY `id_cep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
