
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <title>Consulta de CEP</title>
    </head>
    <body>
        <?php
        require_once './funcoes/DAO/cepCTRL.php';
        require_once './funcoes/DAO/cepVO.php';

        $cepVO = new cepVO();
        $cepCTRL = new cepCTRL();

        //Atribui um valor padrão de inicialização para a variável $cep
        $cep = ['numero_cep' => null,
            'logradouro_cep' => null,
            'bairro_cep' => null,
            'cidade_cep' => null,
            'uf_cep' => null,
            'retorno' => null];

        if (isset($_POST['cep'])) {

            //Verifica se o CEP foi digitado
            if ($_POST['cep'] != '') {
                //Verifica se o CEP é numérico
                if (is_numeric(str_replace("-", "", $_POST['cep'])) == true) {

                    //Seta o valor do CEP
                    $cepVO->setNumeroCep($_POST['cep']);
                    //Faz a primeira consulta no banco de dados para verificar se já existe o CEP
                    $retorno = $cepCTRL->ConsultaCepBaseDados($cepVO);
                    //Verifica o retorno do banco de dados
                    if (!$retorno) {
                        //Tira a mascara do Cep, pois a API funciona apenas com o cep sem máscara
                        $cepSemPontuacao = str_replace("-", "", $_POST['cep']);
                        //Link da API com o cep
                        $url = 'https://viacep.com.br/ws/' . $cepSemPontuacao . '/xml/';

                        // inicia o curl
                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_HEADER, false);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, True);
                        curl_setopt($curl, CURLOPT_URL, $url);
                        //Executa e traz o retorno
                        $return = curl_exec($curl);
                        curl_close($curl);

                        //Verifica o retorno do cep
                        if (!$return) {
                            echo ' <div class = "alert alert-primary" role = "alert">
                        Digite um CEP válido!
                        </div>';
                        } else {
                            //Carrega o xml
                            $xml = simplexml_load_string($return);
                            //Converte para Json
                            $json = json_encode($xml);
                            //Descodifica o Array
                            $array = json_decode($json, TRUE);
                            //Verifica se o array possui algum erro no retorno do CEP
                            if (key_exists('erro', $array)) {
                                echo ' <div class = "alert alert-primary" role = "alert">
                            CEP inválido!
                            </div>';
                            } else {
                                //Set os valore do Array antes de gravar no banco de dados
                                $cepVO->setNumeroCep($array['cep']);
                                //Verifica se é vazio o campo logradouro, pois alguns CEPS não possuem valor de logradouro
                                if (!empty($array['logradouro'])) {
                                    $cepVO->setLogradouroCep($array['logradouro']);
                                } else {
                                    $cepVO->setLogradouroCep("");
                                }
                                //Verifica se é vazio o bairro, pois alguns CEPS não possuem valor de bairro.
                                if (!empty($array['bairro'])) {
                                    $cepVO->setBairroCep($array['bairro']);
                                } else {
                                    $cepVO->setBairroCep("");
                                }
                                $cepVO->setCidadeCep($array['localidade']);
                                $cepVO->setUfCep($array['uf']);
                                //Inseri os dados da consulta da API no banco de dados.
                                $ret = $cepCTRL->InserirCep($cepVO);
                                ///Faz a consulta do banco de dados e retorna os dados do CEP
                                $cep = $cepCTRL->ConsultaCepBaseDados($cepVO);
                                $cep = $cep[0];
                                //Campo do array para saber se o retorno é da API ou do banco de dados
                                $cep['retorno'] = "api";
                            }
                        }
                    } else {
                        //Retorno do banco de dados
                        $cep = $retorno[0];
                        //Campo do array para saber se o retorno é da API ou do banco de dados
                        $cep['retorno'] = "db";
                    }
                } else {
                     echo ' <div class = "alert alert-primary" role = "alert">
                    Digite um CEP válido!
                    </div>';
                }
            } else {
                echo ' <div class = "alert alert-primary" role = "alert">
                Digite um CEP para realizar a consulta!
                </div>';
            }
        }
        ?>
        <div class="container">
            <form  action="index.php" method="POST">
                <h1 style="">Consulta de CEP</h1>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input  type="text" class="col-md-6" id="cep" name="cep" value="<?= $cep['numero_cep'] ?>" onkeypress="mascara(this, '#####-###')" placeholder="Ex.: 00000-000" maxlength="9" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="logradouro">Logradouro</label>
                    <input type="text" class="col-md-6" id="logradouro" name="logradouro" value="<?= $cep['logradouro_cep'] ?>"  placeholder="Digite aqui o logradouro">
                </div>
                <div class="form-group">
                    <label for="bairro">Bairro</label>
                    <input  type="text" class="col-md-6" id="bairro" name="bairro"value="<?= $cep['bairro_cep'] ?>"  placeholder="Digite aqui o bairro">
                </div>
                <div class="form-group">
                    <label for="localidade">Cidade</label>
                    <input  type="text" class="col-md-6" id="localidade" name="cidade" value="<?= $cep['cidade_cep'] ?>" placeholder="Digite aqui a cidade">
                </div>
                <div class="form-group">
                    <label for="uf">UF</label>
                    <input type="text" class="col-md-6" id="uf" name="uf" value="<?= $cep['uf_cep'] ?>" placeholder="Digite aqui a UF">
                </div>
                <div>
                    <button  id="btnConsultar" type="submit" class="btn btn-primary" onclick="">Consultar</button>
                </div>
            </form>
        </div>
        <script src="funcoes/js/mascaras.js" type="text/javascript"></script>
    </body>
</html>
